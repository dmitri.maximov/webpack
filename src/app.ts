import greeter = require('./greeter');
import $ = require('jquery');
import runtime = require('serviceworker-webpack-plugin/lib/runtime');
import registerEvents = require('serviceworker-webpack-plugin/lib/browser/registerEvents');

console.log('the main JS thread was loaded');

if ('serviceWorker' in navigator && (window.location.protocol === 'https:' ||
    window.location.hostname === 'localhost')) {
  const registration = runtime.register();

  registerEvents(registration, {
    onInstalled: () => {
      console.log('onInstalled');
    },
    onUpdateReady: () => {
      console.log('onUpdateReady');
    },
    onUpdating: () => {
      console.log('onUpdating');
    },
    onUpdateFailed: () => {
      console.log('onUpdateFailed');
    },
    onUpdated: () => {
      console.log('onUpdated');
    },
  });
} else {
  console.log('serviceWorker not available');
}

$(() => {
  $(".header").html(greeter("World"));
});